package b137.irinco.s03a1;

import java.util.Scanner;

public class FactorialProgram {
    public static void main(String[] args){
        System.out.println("Factorial Program");

        // Activity:
        // Create a Java program that accepts an integer and computes for
        // the factorial value and displays it to the console.

        Scanner appScanner = new Scanner(System.in);

        System.out.println("What is the number? ");
        int number = appScanner.nextInt();
        int factor = 1;
        for (int i = 1; i <= number; i++){
            factor = factor*i;

        }
        System.out.println("Factor of " + number + " is " + factor);

    }

}
